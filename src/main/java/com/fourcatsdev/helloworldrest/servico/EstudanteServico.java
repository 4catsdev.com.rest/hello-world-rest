package com.fourcatsdev.helloworldrest.servico;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

import com.fourcatsdev.helloworldrest.modelo.Estudante;

@Service
public class EstudanteServico {

	public List<Estudante> buscarTodosEstudantes() {
		List<Estudante> estudantes = new ArrayList<Estudante>();
		estudantes.add(new Estudante(1L, "Maria da Silva", 30));
		estudantes.add(new Estudante(2L, "João da Silva", 35));
		estudantes.add(new Estudante(3L, "Fernanda Costa", 20));
		return estudantes;
	}

}
