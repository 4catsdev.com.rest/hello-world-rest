package com.fourcatsdev.helloworldrest.controle;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fourcatsdev.helloworldrest.modelo.Estudante;
import com.fourcatsdev.helloworldrest.servico.EstudanteServico;


@RestController
@RequestMapping("api/estudantes")
public class EstudanteControle {

	@Autowired
	private EstudanteServico estudanteServico;

	// http://localhost:8080/api/estudantes
	@GetMapping
	public ResponseEntity<List<Estudante>> buscarTodosEstudantes() {
		List<Estudante> estudantes = estudanteServico.buscarTodosEstudantes();
		return new ResponseEntity<>(estudantes, HttpStatus.OK);
	}

}
